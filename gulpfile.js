const fs = require('fs');
const gulp = require('gulp');
const log = require('color-log');
var vendor_file = "./";

/**
 * Setup Libraries
 * Copy from bower_components to Libraries
 */

var build = {
    libraries: "./libraries/",
    base: "./core/",
};

gulp.task('setup-libraries', function () {
    log.info("Setup Libraries");
    return gulp.src(require(vendor_file + 'libraries.json'), {base: 'bower_components'})
        .pipe(gulp.dest(build.libraries));
});

gulp.task('setup-base', function () {
    log.info("Setup Libraries");
    return gulp.src(require(vendor_file + 'core.json'), {base: 'bower_components'})
        .pipe(gulp.dest(build.base));
});